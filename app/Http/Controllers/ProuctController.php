<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class ProuctController extends Controller
{
    function list(){

        $data_product = Product::all();
        return view("product-list")
        ->with("data_product",$data_product);
    }

    function create(){
        return view('product-create');
    }

    function save(Request $request){
        $data_product = Product::create([
            "Kategori"=>$request->input("Kategori"),
            "Nama"=>$request->input("Nama"),
            "Harga"=>$request->input("Harga"),
        ]);

        if($data_product){
            return redirect(url("product"))
            ->with("status","sukses");
        }else{
            return redirect(url("product"))
            ->with("status","gagal");
        }
    }

    function edit($id){
        $data_product = Product::find($id);
        return view("product-edit")
        ->with("data_product", $data_product);
    }

    function update($id, Request $request){
        $data_product = Product::find($id);
        $data_product ->Kategori = $request->input("Kategori");
        $data_product ->Nama =$request->input("Nama");
        $data_product ->Harga = $request->input("Harga");
        {
        $data_product->save();
        return redirect (url("product"));
        }
    }

    function delete($id){
        $data_product = Product::find($id);
        $data_product -> delete();
        return redirect(url("product"));
    }
}
