<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;

class Categori extends Controller
{
    function list(Request $request){
        

        $ordering = $request -> input("ordering");
        $ut = $request -> input ("ut");
        $search = $request -> input("search");
        $filter = $request -> input("filter");
        // if($search!=""){
            if($filter=="1" && $search==""){
                $data_kategori = kategori::where("kategori","like","%".$search."%")
                                        ->orderBy($ordering, $ut)->paginate(5);
            }
            elseif($filter=="1" && $ut=='' && $ordering==''){
                $data_kategori = kategori::where("kategori","like","%".$search."%")
                                        ->orderBy("kategori", 'asc')->paginate(5);
            }
            elseif($filter=="1" && $ordering==""){
                $data_kategori = kategori::where("kategori","like","%".$search."%")
                                        ->orderBy("kategori", $ut)->paginate(5);
            }
            elseif($filter=="1" && $search!=""){
                $data_kategori = kategori::where("kategori","like","%".$search."%")
                                        ->orderBy($ordering, $ut)->paginate(5);
            }
            else{
                $data_kategori = kategori::paginate(5);
            }

        return view("categori-list")
            ->with("data_kategori",$data_kategori);

        // $data_kategori = Kategori::query();
        // if($ordering!="" && $ut!=""){
        //     $data_kategori = $data_kategori ->orderBy($ordering, $ut);
        // }
        // if($ordering!="" && $ut ==""){
        //     $data_kategori = $data_kategori ->orderBy($ordering, "asc");
        // }
        // if($ordering =="" && $ut!=""){
        //     $data_kategori = $data_kategori ->orderBy("kategori", $ut);
        // }
        // if($search!=""){
        //     $data_kategori = $data_kategori ->where("kategori", "like", "%".$search."%");
        // }
        // else{
        //     $data_kategori = $data_kategori-> paginate(5);
        // }
        
        
    
    }

    function create(){
        return view('category-create');
    }

    function save(Request $request){
        $data_kategori = Kategori::create([
            "kategori"=>$request->input("kategori"),
            "slug"=>$request->input("slug"),
            "urutan"=>$request->input("urutan"),
            "status"=>$request->input("status"),
        ]);

        if($data_kategori){
            return redirect(url("categori"))
            ->with("status","sukses");
        }else{
            return redirect(url("categori"))
            ->with("status","gagal");
        }
    }

    function edit($id){
        $data_kategori = Kategori::find($id);
        return view("category-edit")
        ->with("data_kategori", $data_kategori);
    }

    function update($id, Request $request){
        $data_kategori = Kategori::find($id);
        $data_kategori ->kategori = $request->input("kategori");
        $data_kategori ->slug =$request->input("slug");
        $data_kategori ->urutan = $request->input("urutan");
        $data_kategori ->status = $request->input("status");
        {
        $data_kategori->save();
        return redirect (url("categori"));
        }
    }

    function delete($id){
    $data_kategori = Kategori::find($id);
    $data_kategori-> delete();
    return redirect (url("categori"));
    }
}
