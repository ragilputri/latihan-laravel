<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
    <title>Categori-List</title>
    <style>
    .table{
      width: 50%;
      border: 1px solid black;
    }
    thead{
      text-align: center;
      background-color:  #4CAF50;
      color: white;
    }
    tr:nth-child(even) {background-color: #f2f2f2;}
    </style>
</head>
<body>
<!--Search-->
<div class="col-md">
<form class="d-flex" action="{{url('categori')}}">
    <input type="search" name="search" value="" placeholder="Search" aria-label="Search" >
    <select name="ordering" aria-label="Default select example" placeholder="Urutan Berdasarkan">Urutan Berdasarkan
      <option value="">Urutan Berdasarkan</option>
      <option value="kategori" name="kategori">Kategori</option>
      <option value="urutan" name="urutan">Urutan Nomor</option>
    </select>
    <select name="ut" aria-label="Default select example" placeholder="Urutan">
      <option value="">Urutan</option>
      <option value="asc" name="asc">ASC</option>
      <option value="desc" name="desc">DESC</option>
    </select>
    <button class="btn btn-outline-success" type="submit" name="filter" value='1'>Filter</button>
</form>
</div>
<br>
<table class="table table-bordered">
  <thead class="table-active">
  <tr>
  <th scope="col">Nama Kategori</th>
  <th scope="col">Slug</th>
  <th scope="col">Urutan</th>
  <th scope="col">Status</th>
  <th scope="col">Aksi</th>
  </tr>
  </thead>
  @foreach($data_kategori as $kt)
  <tr>
  <td>{{$kt->kategori}}</td>
  <td>{{$kt->slug}}</td>
  <td>{{$kt->urutan}}</td>
  <td>{{$kt->status}}</td>
  <td>
  <a href="/categori/edit/{{$kt->id}}"><button type="submit" class="btn btn-success btn-sm">Edit</button></a>
  <a href="/categori/delete/{{$kt->id}}"><button type="submit" class="btn btn-danger btn-sm">Delete</button></a>
  </td>
  </tr>
  @endforeach
  </table>
{{$data_kategori->appends(Request::all())->links()}} 
</body>
</html>