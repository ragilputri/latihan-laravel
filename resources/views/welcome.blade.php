<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">
        <!--Bootstrap-->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

        <!--Css-->
        <style>
        .button1{border-radius: 12px;}
        .button{
          text-align: center;
          padding: 180px;
        }
        </style>
    </head>
    <body>
    <div class="card text-white" style="height: 70px;">
  <img src="https://images5.alphacoders.com/107/thumb-1920-1072470.jpg" class="card-img" alt="furniture/img">
  <div class="card-img-overlay">
  <ul class="nav justify-content-center">
  <li class="nav-item">
    <a class="nav-link active" aria-current="page" href="/">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="https://laravel.com/docs">Docs</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="https://laravel-news.com">News</a>
  </li>
</ul>
    <div class="button">
    <h2 class="card-title" style="margin:25px;">Let's Make Home Happy</h2>
    <a href="/product"><button class="btn btn-primary">Product</button></a>
    <a href="/categori"><button class="btn btn-primary">Categori</button></a>
    <a href="/categori/create"><button class="btn btn-primary">Create Category</button></a>
    <a href="/product/create"><button class="btn btn-primary">Create Product</button></a>
    <a href=""> <button class="">Regina Permata</button>
</div>
  </div>
</div>
</body>
</html>
