<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

    <title>product-list</title>
    <style>
    .table{
      width: 60%;
      border: 1px solid black;
    }
    thead{
      text-align: center;
      background-color:  #4CAF50;
      color: white;
    }
    tr:nth-child(even) {background-color: #f2f2f2;}
    </style>
  </head>
  <body>
  
  <table class="table table-bordered">
  <thead class="table-active">
  <tr>
  <th scope="col">No</th>
  <th scope="col">Categori</th>
  <th scope="col">Nama</th>
  <th scope="col">Harga</th>
  <th scope="col">Aksi</th>
  </tr>
  </thead>
  <tbody>
  @foreach($data_product as $row)
  <tr>
  <td>{{$row->id}}</td>
  <td>{{$row->Kategori}}</td>
  <td>{{$row->Nama}}</td>
  <td>{{$row->Harga}}</td>
  <td>
  <a href="/product/edit/{{$row->id}}"><button type="submit" class="btn btn-success btn-sm">Edit</button></a>
  <a href="/product/delete/{{$row->id}}"><button type="submit" class="btn btn-danger btn-sm">Delete</button></a>
  </td>
  </tr>
  @endforeach
  </tbody>
  </table>
  
    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.4/dist/umd/popper.min.js" integrity="sha384-q2kxQ16AaE6UbzuKqyBE9/u/KzioAlnx2maXQHiDX9d4/zp8Ok3f+M7DPm+Ib6IU" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.min.js" integrity="sha384-pQQkAEnwaBkjpqZ8RU1fF1AKtTcHJwFl3pblpTlHXybJjHpMYo79HY3hIi4NKxyj" crossorigin="anonymous"></script>
    -->
  </body>
</html>