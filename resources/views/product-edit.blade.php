<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Product-Edit</title>
</head>
<body>
<div class="Form">
    <h2><label>Edit Data Product</label></h2>
   <form method="POST" action= "{{url('')}}/product/update/{{$data_product->id}}">
   @csrf
   <table>
   <tr>
        <td><label for="nama">Kategori Product</label></td>
        <td>:</td>
        <td><input type="text" name="Kategori" value="{{$data_product->Kategori}}"></td>
    </tr>
    <tr>
        <td><label for="nama">Nama Product</label></td>
        <td>:</td>
        <td><input type="text" name="Nama" value="{{$data_product->Nama}}"></td>
    </tr>
    <tr>
        <td><label for="nama">Harga</label></td>
        <td>:</td>
        <td><input type="text" name="Harga" value="{{$data_product->Harga}}"></td>
    </tr>
    <tr>
    <td><input type="submit" value="Simpan Data" name="simpan"></td>
    </tr>   
</table> 
</form>
</div>
</body>
</html>