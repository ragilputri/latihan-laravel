<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Categori-Edit</title>
</head>
<body>
<div class="Form">
    <h2><label>Update Data Category</label></h2>
   <form method="POST" action= "{{url('')}}/categori/update/{{$data_kategori->id}}">
   @csrf
    <table>
   <tr>
        <td><label for="nama">Nama Kategori</label></td>
        <td>:</td>
        <td><input type="text" name="kategori" value="{{$data_kategori->kategori}}"></td>
    </tr>
    <tr>
        <td><label for="nama">Slug</label></td>
        <td>:</td>
        <td><input type="text" name="slug" value="{{$data_kategori->slug}}"></td>
    </tr>
    <tr>
        <td><label for="nama">Urutan</label></td>
        <td>:</td>
        <td><input type="text" name="urutan" value="{{$data_kategori->urutan}}"></td>
    </tr>
    <tr>
        <td><label for="nama">Status</label></td>
        <td>:</td>
        <td><input type="text" name="status" value="{{$data_kategori->status}}"></td>
    </tr>
    <tr>
    <td><input type="submit" value="Update Data" name="simpan"></td>
    </tr>   
</table> 
</form>
</div>
</body>
</html>