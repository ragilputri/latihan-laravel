<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Category-Create</title>
    <style>
    .Form{
        padding: 15px;

    }
    </style>
</head>
<body>
<div class="Form">
    <h2><label for="ini">Input Data Category</label></h2>
   <form method="POST" action="{{url('categori/save')}}">
   @csrf
    <table>
   <tr>
        <td><label for="nama">Nama Kategori</label></td>
        <td>:</td>
        <td><input type="text" name="kategori"></td>
    </tr>
    <tr>
        <td><label for="nama">Slug</label></td>
        <td>:</td>
        <td><input type="text" name="slug"></td>
    </tr>
    <tr>
        <td><label for="nama">Urutan</label></td>
        <td>:</td>
        <td><input type="text" name="urutan"></td>
    </tr>
    <tr>
        <td><label for="nama">Status</label></td>
        <td>:</td>
        <td><input type="text" name="status"></td>
    </tr>
    <tr>
    <td><input type="submit" value="Simpan Data" name="simpan"></td>
    </tr>   
</table> 
</form>
</div>
</body>
</html>