<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('product','ProuctController@list');
Route::get('categori','Categori@list');

Route::get('categori/create','Categori@create');
Route::post('categori/save','Categori@save');
Route::get('categori/edit/{id}','Categori@edit');
Route::post('categori/update/{id}','Categori@update');
Route::get('categori/delete/{id}','Categori@delete');

Route::get('product/create','ProuctController@create');
Route::post('product/save','ProuctController@save');
Route::get('product/edit/{id}','ProuctController@edit');
Route::post('product/update/{id}','ProuctController@update');
Route::get('product/delete/{id}','ProuctController@delete');



